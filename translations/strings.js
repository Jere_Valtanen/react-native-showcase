export const translations = {
       "en": {
              "HomeScreenTitle": "Home screen",
              "SkillsScreenTitle": "Skills",
              "VeganScannerScreenTitle": "Vegan scanner",
              "ControlPanelScreenTitle": "Control panel",
              "WhoAmiTitle": "Who am I",
              "WhoAmiContent": "I'm a software developer armed with a varied full-stack skillset.",
              "Settings": "Settings",
              "Language": "Language",
              "FontSize": "Text size",
              "Skills": "Skills",
              "FetchingData": "Fetching data...",
              "JobExperience": "Job experience",
              "Extra": "Additional info",
              "VeganScannerInfo": "Vegan scanner is an application made to ease the life of vegans and people close to them.\nBy scanning a product's barcode you get an immediate result on whether or not the product suits your diet. Made with React Native",
              "ControlPanelInfo": "The Control panel was created for controlling the lighting of my living room. I also added some perks like a drawing board and coronavirus stats.",
              "Jere": "Jere Valtanen"
       },
       "fi": {
              "HomeScreenTitle": "Kotiruutu",
              "SkillsScreenTitle": "Osaaminen",
              "VeganScannerScreenTitle": "Vegaaniskanneri",
              "ControlPanelScreenTitle": "Ohjauspaneeli",
              "WhoAmiTitle": "Kuka olen",
              "WhoAmiContent": "Olen monipuolisella full-stack osaamisella varustettu ohjelmistokehittäjä. Tiimityöskentelijänä sekä osaajana olen marinoitunut monipuolisten projektien parissa. Tiimissä pyrin ottamaan kaikki huomioon. En pelkää pyytää apua ja annan sitä mielelläni. Olen nopea omaksumaan uusia asioita ja se antaa minulle rohkeutta ottaa vastaan uusia haasteita.\n\nErityisen kiinnostunut olen mobiilikehityksestä, jota töiden lisäksi harrastan aktiivisesti myös vapaa-ajallani.",
              "Settings": "Asetukset",
              "Language": "Kieli",
              "FontSize": "Tekstin koko",
              "Skills": "Osaaminen",
              "FetchingData": "Haetaan tietoja...",
              "JobExperience": "Työkokemus",
              "Extra": "Lisätietoja",
              "VeganScannerInfo": "Vegaaniskanneri on sovellus tehty helpottamaan vegaanien ja heidän läheistensä elämää.\nTuotteen viivakoodin skannaamalla saat välittömän tuloksen tuotteen sopivuudesta ruokavalioosi. Toteutettu React Nativella",
              "ControlPanelInfo": "Tein kotiprojektina ohjauspaneelin, jolla voi hallita olohuoneen valaistusta. Lisäksi paneelista löytyy piirtotaulu jo koronavirus tilastoja. Alustana Raspberry Pi.\nTeknologiat; React, Express JS, MQTT, Python",
              "Jere": "Jere Valtanen"
       }
}