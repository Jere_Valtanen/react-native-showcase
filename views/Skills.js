import { observer } from 'mobx-react';
import React, { useState, useEffect } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import SkillListItem from '../components/SkillListItem';
import { useLocalizationStore } from "../store/localizationStore";
import { useSettingsStore } from "../store/settingsStore";
import { skillsStyles } from "../styles/skillsStyles"
import { sharedStyles } from "../styles/sharedStyles";

const Skills = observer((props, { navigation }) => {
    const localizationStore = useLocalizationStore();
    const settingsStore = useSettingsStore();
    const [fetchingData, setFetchingData] = useState(true)
    const [skills, setSkills] = useState([])

    useEffect(() => {
        async function fetchData() {
            try {
                setFetchingData(true)
                const res = await fetch("https://react-native-showcase-api.herokuapp.com/skills/" + localizationStore.getSelectedLanguage.value)
                const data = await res.json()
                setFetchingData(false)
                console.log(data)
                setSkills(data)
            }
            catch (e) {
                console.log("Error while fetching data! " + e)
                setFetchingData(false)
            }
        }
        fetchData()
    }, [])

    const renderSkill = ({ item }) => {
        console.log(item)
        return (
            <SkillListItem title={item.title} imageUrl={item.image_url} jobExperience={item.job_experience} extra={item.extra} />
        )
    }

    return (
        <View style={sharedStyles.mainView}>
            {!fetchingData ?
                <View style={{ width: '100%', justifyContent: "center", alignItems: "center" }}>
                    <FlatList
                        style={skillsStyles.skillList}
                        data={skills}
                        renderItem={renderSkill}
                        keyExtractor={item => item.title}
                    />
                </View>
                : <View>
                    <ActivityIndicator size="large" color="red" />
                    <Text>{localizationStore.getLocalString("FetchingData")}</Text>
                </View>}
        </View>
    )
})

export default Skills