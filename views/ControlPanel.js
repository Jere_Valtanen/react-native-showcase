import { observer } from 'mobx-react';
import React, { } from 'react';
import { View, Text, Image, StyleSheet, Linking } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Swiper from 'react-native-swiper'
import { useLocalizationStore } from "../store/localizationStore";
import { useSettingsStore } from "../store/settingsStore";
import { WebView } from 'react-native-webview';
import { sharedStyles } from "../styles/sharedStyles";

const ControlPanel = observer((props, { }) => {
    const localizationStore = useLocalizationStore();
    const settingsStore = useSettingsStore();

    const goToUrl = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }

    return (
        <View style={sharedStyles.mainView}>
            <Swiper height={'50%'} style={styles.wrapper} showsButtons={true} activeDotColor={'#bd5044'}>
                <View style={styles.slide}>
                    <Image source={{ uri: "https://i.imgur.com/6xJBNlw.png" }} style={styles.image} />
                </View>
                <View style={styles.slide}>
                    <Image source={{ uri: "https://i.imgur.com/T1UHugV.png" }} style={styles.image} />
                </View>
                <View style={styles.slide}>
                    <Image source={{ uri: "https://i.imgur.com/Q2uS5uU.png" }} style={styles.image} />
                </View>
                <View style={styles.slide}>
                    <Image source={{ uri: "https://i.imgur.com/krhBplt.png" }} style={styles.image} />
                </View>
                <View style={styles.slide}>
                <WebView
                    automaticallyAdjustContentInsets={false}
                    source={{ html: '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/KfG0cZe2NWc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' }}
                    style={{width:300, height: 169, marginTop: 20 }}
                />
                </View>
                <View style={styles.slide}>
                <WebView
                    automaticallyAdjustContentInsets={false}
                    source={{ html: '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/4atpOJbHZaY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' }}
                    style={{width:300, height: 169, marginTop: 20 }}
                />
                </View>
            </Swiper>
            <View style={{ width: '100%' }}>
                <Text
                    style={{
                        marginLeft: 30, marginRight: 20, marginVertical: 20, justifyContent: 'center', alignItems: 'center', color: 'black',
                        fontSize: settingsStore.getFontSize
                    }}>
                    {localizationStore.getLocalString("ControlPanelInfo")}</Text>
            </View>
        </View>
    )
})

const styles = StyleSheet.create({
    wrapper: {
        marginTop: 10, justifyContent: "center", alignItems: "center"
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        height: '100%',
        width: '100%',
        resizeMode: "contain"
    },
    button: {
        padding: 10,
        width: 150,
        justifyContent: "center",
        alignItems: "center",
        margin: 'auto',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "blue",
        marginHorizontal: 20
    }
})

export default ControlPanel