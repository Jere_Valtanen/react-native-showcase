import React from 'react';
import { observer } from 'mobx-react';
import Main from './Main';
import Settings from './Settings';
import Skills from './Skills';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useLocalizationStore } from '../store/localizationStore';
import VeganScanner from './VeganScanner';
import ControlPanel from './ControlPanel';
import Icon from 'react-native-vector-icons/FontAwesome5';

const Tab = createBottomTabNavigator();

const Navigator = observer(() => {
  const localizationStore = useLocalizationStore();

  return (
    <Tab.Navigator initialRouteName={"Home"}       
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;

        if (route.name === 'Home') {
          iconName = 'home'
        } else if (route.name === 'Settings') {
          iconName = 'cog';
        } else if (route.name === 'Skills') {
          iconName = 'user-tie'
        } else if (route.name === 'VeganScanner') {
          iconName = 'blender' 
        } else if (route.name === 'ControlPanel') {
          iconName = 'solar-panel'
        }
        return <Icon name={iconName} size={size} color={color} />;
      },
    })}
    tabBarOptions={{
      activeTintColor: 'black',
      inactiveTintColor: '#EAE7DC',
      activeBackgroundColor: '#bd5044',
      inactiveBackgroundColor: '#bd5044',
    }}>
      <Tab.Screen name="Home" component={Main} options={{ title: localizationStore.getLocalString("HomeScreenTitle") }} />
      <Tab.Screen name="Skills" component={Skills} options={{title: localizationStore.getLocalString("SkillsScreenTitle")}} />
      <Tab.Screen name="VeganScanner" component={VeganScanner} options={{title: localizationStore.getLocalString("VeganScannerScreenTitle")}} />
      <Tab.Screen name="ControlPanel" component={ControlPanel} options={{title: localizationStore.getLocalString("ControlPanelScreenTitle")}} />
      <Tab.Screen name="Settings" component={Settings} options={{ title: localizationStore.getLocalString("Settings") }} />    
    </Tab.Navigator>
  );
})

export default Navigator;