import React from 'react';
import { View, ScrollView, Text, Image } from 'react-native';
import { useLocalizationStore } from "../store/localizationStore";
import { useSettingsStore } from "../store/settingsStore";
import { observer } from 'mobx-react';
import { mainStyles } from '../styles/mainStyle';
import { sharedStyles } from "../styles/sharedStyles";

const Main = observer(({ navigation }) => {
    const localizationStore = useLocalizationStore();
    const settingsStore = useSettingsStore();

    return (
        <View style={sharedStyles.mainView}>
            <Text style={[sharedStyles.titleText, {fontSize: settingsStore.getFontSize * 1.25}]}>{localizationStore.getLocalString("Jere")}</Text>
            <View style={mainStyles.profileImageContainer}>
                <Image width={'30%'} height={'30%'} style={mainStyles.profileImage} source={require('../assets/jere.png')} />
            </View>
            <ScrollView style={mainStyles.textContainer}>
            <Text style={[sharedStyles.titleText, {fontSize: settingsStore.getFontSize * 1.25}]}>{localizationStore.getLocalString("WhoAmiTitle")}</Text>
            <Text style={[{fontSize: settingsStore.getFontSize}]}>{localizationStore.getLocalString("WhoAmiContent")}</Text>
            </ScrollView>
        </View>
    );
})

export default Main;