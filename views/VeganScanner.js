import { observer } from 'mobx-react';
import React, { } from 'react';
import { View, Text, Image, StyleSheet, Linking, TouchableOpacity } from 'react-native';
import Swiper from 'react-native-swiper'
import { useLocalizationStore } from "../store/localizationStore";
import { useSettingsStore } from "../store/settingsStore";
import { sharedStyles } from "../styles/sharedStyles";

const VeganScanner = observer((props, { }) => {
    const localizationStore = useLocalizationStore();
    const settingsStore = useSettingsStore();

    const goToUrl = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }

    return (
        <View style={sharedStyles.mainView}>
            <Swiper height={'50%'} style={styles.wrapper} showsButtons={true} activeDotColor={'#bd5044'}>
                <View style={styles.slide}>
                    <Image source={{ uri: "https://i.imgur.com/jkDNGVC.png" }} style={styles.image} />
                </View>
                <View style={styles.slide}>
                    <Image source={{ uri: "https://i.imgur.com/A7K5lsz.png" }} style={styles.image} />
                </View>
                <View style={styles.slide}>
                    <Image source={{ uri: "https://i.imgur.com/u0mgZWe.png" }} style={styles.image} />
                </View>
                <View style={styles.slide}>
                    <Image source={{ uri: "https://i.imgur.com/uMFBUbX.png" }} style={styles.image} />
                </View>
            </Swiper>
            <View style={{ width: '100%' }}>
                <Text
                    style={{
                        marginLeft: 30, marginRight: 20, marginVertical: 20, justifyContent: 'center', alignItems: 'center', color: 'black',
                        fontSize: settingsStore.getFontSize
                    }}>
                    {localizationStore.getLocalString("VeganScannerInfo")}</Text>
                <View style={{ width: '100%', marginBottom: 5, flexDirection: "row", alignContent: "center", justifyContent: "center" }}>
                    <TouchableOpacity
                        onPress={() => goToUrl("https://play.google.com/store/apps/details?id=com.vegaaniskanneri")}
                        style={styles.button}>
                        <Text style={{fontSize: settingsStore.getFontSize}}>Android</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => goToUrl("https://apps.apple.com/fi/app/vegaaniskanneri/id1495675590")}
                        style={styles.button}>
                        <Text style={{fontSize: settingsStore.getFontSize}}>iOS</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
})

const styles = StyleSheet.create({
    wrapper: {
        marginTop: 10, justifyContent: "center", alignItems: "center"
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },
    image: {
        height: '100%',
        width: '100%',
        resizeMode: "contain"
    },
    button: {
        padding: 10,
        width: 150,
        justifyContent: "center",
        alignItems: "center",
        margin: 'auto',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "#bd5044",
        marginHorizontal: 20
    }
})

export default VeganScanner