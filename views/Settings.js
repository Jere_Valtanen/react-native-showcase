import React from 'react';
import { View, Text } from 'react-native';
import { useLocalizationStore } from "../store/localizationStore";
import { useSettingsStore } from "../store/settingsStore";
import { observer } from 'mobx-react';
import { sharedStyles } from '../styles/sharedStyles';

import LanguagePicker from '../components/LanguagePicker';
import FontSizeSlider from '../components/FontSizeSlider';

const Settings = observer(({ navigation }) => {
    const localizationStore = useLocalizationStore();
    const settingsStore = useSettingsStore();

    return (
        <View style={sharedStyles.mainView}>
            <Text style={[sharedStyles.titleText, {fontSize: settingsStore.getFontSize * 1.25}]}>{localizationStore.getLocalString("Settings")}</Text>
            <LanguagePicker />
            <FontSizeSlider />
        </View>
    );
})

export default Settings;