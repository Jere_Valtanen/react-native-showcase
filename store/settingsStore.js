import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { observable, action, makeObservable } from 'mobx';

export default class SettingsStore {
    fontSize = 18

    constructor() {
        makeObservable(this, {
            fontSize: observable,
            setFontSize: action
        })
        this.fetchFontSizeFromStorage()
    }

    fetchFontSizeFromStorage() {
        AsyncStorage.getItem('@font_Size')
            .then((newSize) => {
                if (newSize !== null)
                    this.fontSize = parseInt(newSize)
            })
            .catch((e) => console.log("Failed to fetch font size!" + e))
    }

    get getFontSize() {
        return this.fontSize;
    }

    get getLargeFontSize() {
        return this.fontSize * 1.25;
    }

    setFontSize(newSize) {
        this.fontSize = newSize;
        AsyncStorage.setItem('@font_Size', this.fontSize.toString())
            .then(() => console.log("Successfully saved font size"))
            .catch(() => console.log("Failed to save font size!"))
    }
}

const SettingsStoreContext = React.createContext()

export const SettingsStoreProvider = ({ children, store }) => {
    return (
        <SettingsStoreContext.Provider value={store}>{children}</SettingsStoreContext.Provider>
    )
}

export const useSettingsStore = () => React.useContext(SettingsStoreContext);