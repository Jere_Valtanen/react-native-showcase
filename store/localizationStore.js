import * as RNLocalize from "react-native-localize";
import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { observable, computed, action, makeObservable } from 'mobx';
import { translations } from '../translations/strings';

export default class LocalizationStore {
    languages = [
        {
            name: "English", value: "en", localName: "English"
        },
        {
            name: "Finnish", value: "fi", localName: "Suomi"
        },
    ];
    selectedLanguage = this.languages[0];

    constructor() {
        makeObservable(this, {
            selectedLanguage: observable,
            setLanguage: action,
        })
        this.fechInitialLanguage();
    }

    fechInitialLanguage() {
        AsyncStorage.getItem('@language')
            .then((newLanguage) => {
                if (newLanguage !== null) {
                    console.log("Successfully loaded language from async storage")
                    this.setLanguage(JSON.parse(newLanguage).value)
                }
                else {
                    console.log("No language in async storage. Setting based on locale")
                    this.languageFromLocale()
                }
            })
            .catch((e) => console.log("Failed to fetch font size!" + e))

    }

    languageFromLocale() {
        let locales = RNLocalize.getLocales()
        for (let i = 0; i < locales.length; i++) {
            if (this.languages.find(lang => lang.value == locales[i].languageCode)) {
                return (this.setLanguage(locales[i].languageCode))
            }
            else {
                console.log("Device locale ", locales[i].languageCode, "is not supported!")
            }
        }
    }

    get getLanguages() {
        return this.languages;
    }

    get getSelectedLanguage() {
        return this.selectedLanguage;
    }

    setLanguage(lang) {
        this.selectedLanguage = this.languages.find(target => target.value == lang);
        AsyncStorage.setItem('@language', JSON.stringify(this.selectedLanguage))
            .then(() => console.log("Successfully saved language"))
            .catch((e) => console.log("Failed to save language! ", e))
    }

    getLocalString(string) {
        return translations[this.getSelectedLanguage.value][string];
    }
}

const LocalizationStoreContext = React.createContext()

export const LocalizationStoreProvider = ({ children, store }) => {
    return (
        <LocalizationStoreContext.Provider value={store}>{children}</LocalizationStoreContext.Provider>
    )
}

export const useLocalizationStore = () => React.useContext(LocalizationStoreContext);