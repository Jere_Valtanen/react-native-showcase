import React from 'react';
import { View, Text } from 'react-native';
import { useLocalizationStore } from "../store/localizationStore";
import { useSettingsStore } from "../store/settingsStore";
import { observer } from 'mobx-react';
import { Picker } from '@react-native-picker/picker';
import { settingsStyles } from '../styles/settingsStyle';

const LanguagePicker = observer(() => {
    const localizationStore = useLocalizationStore();
    const settingsStore = useSettingsStore();

    return (
        <View style={{ width: '100%' }}>
            <View style={settingsStyles.itemContainer}>
                <Text style={{ width: '40%', fontSize: settingsStore.getFontSize }}>{localizationStore.getLocalString("Language")}</Text>
                <Picker
                    selectedValue={localizationStore.getSelectedLanguage.value}
                    style={{ height: 50, width: '40%' }}
                    prompt={localizationStore.getLocalString("Language")}
                    onValueChange={(itemValue, itemIndex) =>
                        localizationStore.setLanguage(itemValue)}>
                    {localizationStore.getLanguages.map((language, index) => {
                        return <Picker.Item key={index} label={language.localName} value={language.value} />
                    })}
                </Picker>
            </View>
        </View>
    );
})

export default LanguagePicker;