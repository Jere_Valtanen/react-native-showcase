import React from 'react';
import { View, Text } from 'react-native';
import { useLocalizationStore } from "../store/localizationStore";
import { useSettingsStore } from "../store/settingsStore";
import { observer } from 'mobx-react';
import { settingsStyles } from '../styles/settingsStyle';
import Slider from '@react-native-community/slider';

const FontSizeSlider = observer(() => {
    const settingsStore = useSettingsStore();
    const localizationStore = useLocalizationStore();

    return (
        <View style={{ width: '100%' }}>
            <View style={settingsStyles.itemContainer}>
                <Text style={{ width: '40%', fontSize: settingsStore.getFontSize }}>{localizationStore.getLocalString("FontSize")}</Text>
                <Slider
                    style={{ width: '40%', height: 50 }}
                    value={settingsStore.getFontSize}
                    minimumValue={16}
                    maximumValue={23}
                    minimumTrackTintColor="#bd5044"
                    maximumTrackTintColor="#000000"
                    thumbTintColor="#bd5044"
                    step={1}
                    onValueChange={value => settingsStore.setFontSize(value)} />
            </View>
        </View>
    );
})

export default FontSizeSlider;