import React, { useState } from 'react';
import { } from 'react-native';

function HooksTesting(props) {
    const [selectedCharacter, setSelectedCharacter] = useState(1)
    const [chosenSide, setChosenSide] = useState("light")
    const [isDestroyed, setIsDestroyed] = useState(false)

    const sideHandler = side => {
        setChosenSide(side)
    }

    const charSelectHandler = event => {
        const charId = event.target.value;
        setSelectedCharacter(charId)
    }

    const destructionHandler = () => {
        setIsDestroyed(true)
    }

    return (
        <div>

        </div>
    );
}

export default HooksTesting;