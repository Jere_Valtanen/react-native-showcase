import React, { useRef, useState } from 'react';
import { View, Image, Text, StyleSheet, Animated, InteractionManager, TouchableOpacity } from 'react-native';
import { useSettingsStore } from "../store/settingsStore";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { useLocalizationStore } from "../store/localizationStore";

const SkillListItem = ({ title, imageUrl, jobExperience, extra }) => {
    const settingsStore = useSettingsStore();
    const localizationStore = useLocalizationStore();
    const toggleAnim = useRef(new Animated.Value(0)).current
    const toggleOpacity= useRef(new Animated.Value(0)).current
    const [isDetailsVisible, setIsDetailsVisible] = useState(false);

    const animate = () => {
        Animated.timing(
            toggleAnim,
            {
                toValue: isDetailsVisible ? 0 : 180 * (settingsStore.getFontSize / 18),
                duration: 200,
                useNativeDriver: false
            }
        ).start();
        Animated.timing(
            toggleOpacity,
            {
                toValue: isDetailsVisible ? 0 : 1,
                duration: 100,
                useNativeDriver: false
            }
        ).start()
        setIsDetailsVisible(!isDetailsVisible);
    }

    return (
        <TouchableOpacity onPress={animate}>
            <View style={styles.itemContainer}>
                {typeof imageUrl != "undefined" &&
                    <Image style={{ width: '50%', height: 40, resizeMode: "center" }} source={{ uri: imageUrl }} />
                }
                <Icon style={styles.arrowIconStyle} size={25} name="arrow-drop-down" />
                <Text style={[styles.titleText, { fontSize: settingsStore.getFontSize }]}>{title}</Text>
            </View>
            <Animated.View style={[styles.detailsContainer, { height: toggleAnim, opacity: toggleOpacity }]}>
                <View style={styles.experienceContainer}>
                    <Text style={{ fontWeight: "bold", fontSize: settingsStore.getFontSize * 0.85 }}>{localizationStore.getLocalString("JobExperience")}</Text>
                    <Text style={{ fontSize: settingsStore.getFontSize * 0.85 }}>{jobExperience}</Text>
                </View>
                {extra &&
                    <View style={styles.extraContainer}>
                        <Text style={{ fontWeight: "bold", fontSize: settingsStore.getFontSize * 0.85 }}>{localizationStore.getLocalString("Extra")}</Text>
                        <Text style={{ fontSize: settingsStore.getFontSize * 0.85 }}>{extra}</Text>
                    </View>
                }
            </Animated.View>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    itemContainer: {
        width: '100%',
        height: 60,
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        backgroundColor: "#faf1e3",
        marginVertical: 1.5,
        textAlign: "center"
    },
    titleText: {
        width: '50%',
        textAlign: "center",
    },
    detailsContainer: {
        marginHorizontal: 5,
    },
    arrowIconStyle: {
        marginTop: "auto"
    },
    experienceContainer: {
        marginTop: 10,
    },
    extraContainer: {
        marginTop: 10,
        //marginBottom: "auto",
    }
})

export default SkillListItem;