/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { StyleSheet, StatusBar, Alert } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import LocalizationStore, { LocalizationStoreProvider } from './store/localizationStore';
import SettingsStore, { SettingsStoreProvider } from './store/settingsStore';
import Navigator from './views/Navigator';

const localizationStore = new LocalizationStore();
const settingsStore = new SettingsStore();

const App: () => React$Node = () => {

  return (
    <>
    <NavigationContainer>
      <SettingsStoreProvider store={settingsStore}>
        <LocalizationStoreProvider store={localizationStore}>
          <StatusBar backgroundColor="#bd5044" barStyle="dark-content" />
          <Navigator />
        </LocalizationStoreProvider>
      </SettingsStoreProvider>
    </NavigationContainer>
    </>
  );
};

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default App;
