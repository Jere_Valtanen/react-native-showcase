import { StyleSheet } from 'react-native';

export const skillsStyles = StyleSheet.create({
  skillList: {
    width: '100%',
    height: '100%',
  },
  introText: {
    marginVertical: 10,
  }
});