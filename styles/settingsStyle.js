import { StyleSheet } from 'react-native';

export const settingsStyles = StyleSheet.create({
  itemContainer: {
    backgroundColor: "#faf1e3",
    flexDirection: "row",
    justifyContent: "space-around",
    alignContent: "center",
    alignItems: "center",
    width: '100%',
    marginVertical: 2
  }
});