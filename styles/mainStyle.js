import { StyleSheet } from 'react-native';
import { color } from 'react-native-reanimated';

export const mainStyles = StyleSheet.create({
  profileImageContainer: {
    width: 200,
    height: 200,
    justifyContent: "center",
    alignItems: "center",
  },
  profileImage: {
    height: '100%',
    width: '100%',
  },
  textContainer: {
    backgroundColor: '#d9d4cc',
    width: '100%',
    paddingHorizontal: 20,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    marginTop: 10,
  },
  titleText: {
    fontWeight: "bold",
    color: "#E85A4F",
    textAlign: "center",
    marginTop: 5,
    marginBottom: 10,
  }
});