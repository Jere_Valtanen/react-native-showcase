import { StyleSheet } from 'react-native';

export const sharedStyles = StyleSheet.create({
  mainView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EAE7DC"
  },
  titleText: {
    fontWeight: "bold",
    color: "#E85A4F",
    textAlign: "center",
    marginTop: 5,
    marginBottom: 10,
  }
});